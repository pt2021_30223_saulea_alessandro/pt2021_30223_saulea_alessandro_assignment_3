/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * ProductDAO Class
 * @Date: April 17, 2021
 * @Source: https://www.youtube.com/watch?v=DLJCxmW1M4s
 * https://docs.oracle.com/javase/7/docs/api/javax/swing/table/DefaultTableModel.html
 */

package presentation;

import data_access.*;
import model.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;

public class OrderManagement extends JFrame {

	private final JButton editClientBtn;
	private final JButton delClientBtn;
	private final JButton addProdBtn;
	private final JButton editProdBtn;
	private final JButton delProdBtn;
	private final JButton addOrderBtn;
	private final JButton addClientBtn;
	private final JTextField clientNameField;
	private final JTextField prodNameField;
	private final JTextField priceField;
	private final JTextField stockField;
	private final DefaultTableModel clientModel;
	private final DefaultTableModel productModel;
	private final JTextField quantityField;
	private final JTable clientTable;
	private final JTable productTable;

	public OrderManagement() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 563, 460);
		setTitle("Order Management");
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label1 = new JLabel("Clients");
		label1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label1.setBounds(102, 11, 74, 14);
		contentPane.add(label1);
		
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane1.setBounds(10, 151, 254, 226);
		contentPane.add(scrollPane1);

		clientModel = new DefaultTableModel();
		clientModel.setColumnIdentifiers(new String[]{"ID","Name"});
		for (Client client : (new ClientDAO()).findAll())
			clientModel.addRow(new Object[]{client.getId(),client.getName()});
		clientTable = new JTable(clientModel);
		scrollPane1.setViewportView(clientTable);
		
		addClientBtn = new JButton("Add client");
		addClientBtn.setBounds(10, 36, 110, 23);
		contentPane.add(addClientBtn);
		
		JLabel label2 = new JLabel("Name:");
		label2.setBounds(130, 39, 38, 14);
		contentPane.add(label2);

		clientNameField = new JTextField();
		clientNameField.setBounds(178, 37, 86, 20);
		contentPane.add(clientNameField);
		clientNameField.setColumns(10);
		
		editClientBtn = new JButton("Edit client");
		editClientBtn.setBounds(10, 63, 110, 23);
		contentPane.add(editClientBtn);
		
		delClientBtn = new JButton("Delete client");
		delClientBtn.setBounds(10, 90, 110, 23);
		contentPane.add(delClientBtn);
		
		JLabel label3 = new JLabel("Products");
		label3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label3.setBounds(354, 11, 74, 14);
		contentPane.add(label3);
		
		addProdBtn = new JButton("Add product");
		addProdBtn.setBounds(274, 36, 117, 23);
		contentPane.add(addProdBtn);
		
		editProdBtn = new JButton("Edit product");
		editProdBtn.setBounds(274, 63, 117, 23);
		contentPane.add(editProdBtn);
		
		delProdBtn = new JButton("Delete product");
		delProdBtn.setBounds(274, 90, 117, 23);
		contentPane.add(delProdBtn);
		
		JLabel label4 = new JLabel("Name:");
		label4.setBounds(401, 39, 38, 14);
		contentPane.add(label4);
		
		JLabel label6 = new JLabel("Stock:");
		label6.setBounds(401, 93, 46, 14);
		contentPane.add(label6);
		
		prodNameField = new JTextField();
		prodNameField.setBounds(449, 37, 86, 20);
		contentPane.add(prodNameField);
		prodNameField.setColumns(10);
		
		stockField = new JTextField();
		stockField.setBounds(449, 91, 86, 20);
		contentPane.add(stockField);
		stockField.setColumns(10);
		
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane2.setBounds(274, 151, 261, 226);
		contentPane.add(scrollPane2);
		
		productModel = new DefaultTableModel();
		productModel.setColumnIdentifiers(new String[]{"ID","Name","Price","Stock"});
		productTable = new JTable(productModel);
		scrollPane2.setViewportView(productTable);
		
		JLabel label9 = new JLabel("Quantity:");
		label9.setBounds(122, 391, 61, 14);
		contentPane.add(label9);
		
		quantityField = new JTextField();
		quantityField.setBounds(193, 388, 86, 20);
		contentPane.add(quantityField);
		quantityField.setColumns(10);
		
		addOrderBtn = new JButton("Add order");
		addOrderBtn.setBounds(299, 387, 103, 23);
		contentPane.add(addOrderBtn);
		
		JLabel label5 = new JLabel("Price:");
		label5.setBounds(401, 67, 46, 14);
		contentPane.add(label5);
		
		priceField = new JTextField();
		priceField.setBounds(449, 64, 86, 20);
		contentPane.add(priceField);
		priceField.setColumns(10);
		
		JLabel label7 = new JLabel("Select a client:");
		label7.setBounds(90, 126, 86, 14);
		contentPane.add(label7);
		
		JLabel label8 = new JLabel("Select a product:");
		label8.setBounds(354, 126, 110, 14);
		contentPane.add(label8);

		updateClientTable();
		updateProductTable();
		setVisible(true);
	}

	public String getClientName() { return clientNameField.getText(); }

	public int getSelectedClient() {
		int row = clientTable.getSelectedRow();
		clientNameField.setText((String)clientModel.getValueAt(row,1));
		return (int)clientModel.getValueAt(row,0);
	}

	public String getProdName() { return prodNameField.getText(); }

	public float getPrice() { return Float.parseFloat(priceField.getText()); }

	public int getStock() { return Integer.parseInt(stockField.getText()); }

	public int getSelectedProduct(){
		int row = productTable.getSelectedRow();
		prodNameField.setText((String)productModel.getValueAt(row,1));
		priceField.setText(String.valueOf(productModel.getValueAt(row,2)));
		stockField.setText(String.valueOf(productModel.getValueAt(row,3)));
		return (int)productModel.getValueAt(row,0);
	}

	public int getQuantity(){ return Integer.parseInt(quantityField.getText()); }

	public void updateClientTable(){
		clientModel.setRowCount(0);
		for (Client client : (new ClientDAO()).findAll())
			clientModel.addRow(new Object[]{client.getId(), client.getName()});
	}

	public void updateProductTable(){
		productModel.setRowCount(0);
		for (Product product : (new ProductDAO()).findAll())
			productModel.addRow(new Object[]{product.getId(), product.getName(), product.getPrice(),product.getStock()});
	}

	public void addAddClientListener(ActionListener al){ addClientBtn.addActionListener(al); }

	public void addSelectClientListener(MouseAdapter ma) {clientTable.addMouseListener(ma);}

	public void addEditClientListener(ActionListener al){
		editClientBtn.addActionListener(al);
	}

	public void addDeleteClientListener(ActionListener al ){delClientBtn.addActionListener(al);}

	public void addAddProductListener(ActionListener al) {addProdBtn.addActionListener(al);}

	public void addSelectProductListener(MouseAdapter ma) {productTable.addMouseListener(ma);}

	public void addEditProductListener(ActionListener al){editProdBtn.addActionListener(al);}

	public void addDeleteProductListener(ActionListener al){delProdBtn.addActionListener(al);}

	public void addAddOrderListener(ActionListener al){addOrderBtn.addActionListener(al);}
}
