/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * ConnectionFactory Class
 * @Date: April 17, 2021
 * @Source: https://teams.microsoft.com/_?lm=deeplink&lmsrc=homePageWeb&cmpid=WebSignIn#/pdf/viewer/teams/https:~2F~2Fdi
 * datec.sharepoint.com~2Fsites~2FTP274~2FClass%20Materials~2FLaborator~2FTema%203~2FASSIGNMENT_3_SUPPORT_PRESENTATION.p
 * df?threadId=19:4fed5c2e262a4057b1dabca3437d3a19@thread.tacv2&baseUrl=https:~2F~2Fdidatec.sharepoint.com~2Fsites~2FTP2
 * 74&fileId=615bfd47-ecf2-4d39-bf9f-cdc591b3048d&ctx=files&rootContext=items_view&viewerAction=view
 * https://gitlab.com/utcn_dsrl/pt-layered-architecture/-/blob/master/src/main/java/connection/ConnectionFactory.java
 * https://stackoverflow.com/questions/53131321/spring-boot-jdbc-javax-net-ssl-sslexception-closing-inbound-before-receiving-p
 */

package data_access;

import java.sql.*;

public class ConnectionFactory {
    private static final String driver = "com.mysql.cj.jdbc.Driver";
    private static final String dburl = "jdbc:mysql://localhost:3306/order_management?useSSL=false";
    private static final String user = "root";
    private static final String pass = "mysquel1";
    private ConnectionFactory(){
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection createConnection(){
        try {
            return DriverManager.getConnection(dburl,user,pass);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public static Connection getConnection(){
        return (new ConnectionFactory()).createConnection();
    }

    public static void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void close(Statement statement){
        try {
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void close(ResultSet resultSet){
        try {
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


}
