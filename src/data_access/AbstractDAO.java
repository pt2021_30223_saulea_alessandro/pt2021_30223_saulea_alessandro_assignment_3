/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * AbstractDAO Class
 * @Date: April 17, 2021
 * @Source: https://teams.microsoft.com/_?lm=deeplink&lmsrc=homePageWeb&cmpid=WebSignIn#/pdf/viewer/teams/https:~2F~2Fdi
 * datec.sharepoint.com~2Fsites~2FTP274~2FClass%20Materials~2FLaborator~2FTema%203~2FASSIGNMENT_3_SUPPORT_PRESENTATION.p
 * df?threadId=19:4fed5c2e262a4057b1dabca3437d3a19@thread.tacv2&baseUrl=https:~2F~2Fdidatec.sharepoint.com~2Fsites~2FTP2
 * 74&fileId=615bfd47-ecf2-4d39-bf9f-cdc591b3048d&ctx=files&rootContext=items_view&viewerAction=view
 * https://gitlab.com/utcn_dsrl/pt-layered-architecture/-/blob/master/src/main/java/dao/StudentDAO.java
 * https://docs.oracle.com/javase/tutorial/reflect/member/fieldTypes.html
 */

package data_access;

import java.beans.*;
import java.sql.Statement;
import java.util.*;
import java.lang.reflect.*;
import java.sql.*;

public class AbstractDAO<T> {
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        type = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String createInsertQuery(){
        StringBuilder str = new StringBuilder("insert into " + type.getSimpleName() + " (");
        int i = 1;
        for (Field field: type.getDeclaredFields())
            if (!field.getName().equals("id")) {
                field.setAccessible(true);
                if (i>1)
                    str.append(",").append(field.getName());
                else
                    str.append(field.getName());
                ++i;
            }
        str.append(") value (");
        for (i=1;i<type.getDeclaredFields().length;++i)
            if (i>1)
                str.append(",?");
            else
                str.append("?");
        str.append(")");
        return str.toString();
    }

    private List<T> createObjects(ResultSet resultSet){
        List<T> list = new ArrayList<>();
        try {
            while (resultSet.next()){
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()){
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(),type);
                    Method method = pd.getWriteMethod();
                    method.invoke(instance,value);
                }
                list.add(instance);
            }
        } catch (SQLException | InstantiationException | IllegalAccessException | IntrospectionException | InvocationTargetException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public T findById(int id){
        Connection connection = ConnectionFactory.getConnection();
        T toReturn = null;
        try {
            PreparedStatement s = connection.prepareStatement("select * from "+type.getSimpleName()+" where id = "+id);
            ResultSet rs = s.executeQuery();
            toReturn = createObjects(rs).get(0);
            ConnectionFactory.close(rs);
            ConnectionFactory.close(s);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(connection);
        return toReturn;
    }

    public List<T> findAll(){
        Connection connection = ConnectionFactory.getConnection();
        List<T> list = null;
        try {
            PreparedStatement s = connection.prepareStatement("select * from "+type.getSimpleName());
            ResultSet rs = s.executeQuery();
            list = createObjects(rs);
            ConnectionFactory.close(rs);
            ConnectionFactory.close(s);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        ConnectionFactory.close(connection);
        return list;
    }

    public int insert(T object){
        Connection connection = ConnectionFactory.getConnection();
        int insertedId = -1;
        try {
            PreparedStatement s = connection.prepareStatement(createInsertQuery(),Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            for (Field field: object.getClass().getDeclaredFields())
                if (!field.getName().equals("id")) {
                    field.setAccessible(true);
                    if (field.getType().isAssignableFrom(int.class))
                        s.setInt(i,(int)field.get(object));
                    else if (field.getType().isAssignableFrom(float.class))
                        s.setFloat(i,(float)field.get(object));
                    else // String / LocalDateTime
                        s.setString(i,String.valueOf(field.get(object)));
                    ++i;
                }
            s.executeUpdate();
            ResultSet rs = s.getGeneratedKeys();
            if (rs.next())
                insertedId = rs.getInt(1);
            ConnectionFactory.close(s);
            ConnectionFactory.close(rs);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(connection);
        return insertedId;
    }

    public void update(int id,Field field,Object value){
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement s = connection.prepareStatement
                    ("update "+type.getSimpleName()+" set "+field.getName()+" = ? where id = ?");
            if (field.getType().isAssignableFrom(int.class))
                s.setInt(1,(int)value);
            else if (field.getType().isAssignableFrom(float.class))
                s.setFloat(1,(float)value);
            else
                s.setString(1,(String)value);
            s.setInt(2,id);
            s.executeUpdate();
            ConnectionFactory.close(s);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(connection);
    }

    public void delete(Field field,Object value){
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement s = connection.prepareStatement
                    ("delete from "+type.getSimpleName()+" where "+field.getName()+" = ?");
            if (field.getType().isAssignableFrom(int.class))
                s.setInt(1,(int)value);
            else if (field.getType().isAssignableFrom(float.class))
                s.setFloat(1,(float)value);
            else
                s.setString(1,(String)value);
            s.executeUpdate();
            ConnectionFactory.close(s);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(connection);
    }
}
