/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * ProductDAO Class
 * @Date: April 17, 2021
 */

package model;

import java.time.*;

public class Command {
    private int id;
    private final int idclient;
    private final int idproduct;
    private final int quantity;
    private final LocalDateTime date;

    public Command(int idclient, int idproduct, int quantity) {
        this.idclient = idclient;
        this.idproduct = idproduct;
        this.quantity = quantity;
        date = LocalDateTime.now();
    }
}
