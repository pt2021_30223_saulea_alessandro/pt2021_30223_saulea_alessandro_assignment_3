/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * Controller Class
 * @Date: April 17, 2021
 * @Source: https://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html
 * https://docs.oracle.com/javase/7/docs/api/java/io/File.html#createNewFile()
 * https://www.youtube.com/watch?v=DLJCxmW1M4s
 */

package business_logic;

import data_access.*;
import model.*;
import presentation.*;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.time.*;

public class Controller {

    public static void main(String[] args) {
        final int[] selectedClient = {-1};
        final int[] selectedProduct = {-1};

        OrderManagement frame = new OrderManagement();
        ClientDAO clientDAO = new ClientDAO();
        OrderDAO orderDAO = new OrderDAO();
        ProductDAO productDAO = new ProductDAO();

        frame.addAddClientListener(e -> {
            clientDAO.insert(new Client(frame.getClientName()));
            frame.updateClientTable();
        });

        frame.addSelectClientListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectedClient[0] = frame.getSelectedClient();
            }
        });

        frame.addEditClientListener(e -> {
            try {
                if (selectedClient[0] == -1){
                    JOptionPane.showMessageDialog(null,"Select a client!");
                    return;
                }
                clientDAO.update(selectedClient[0],Client.class.getDeclaredField("name"), frame.getClientName());
            } catch (NoSuchFieldException noSuchFieldException) {
                noSuchFieldException.printStackTrace();
            }
            frame.updateClientTable();
        });

        frame.addDeleteClientListener(e -> {
            try {
                if (selectedClient[0] == -1){
                    JOptionPane.showMessageDialog(null,"Select a client!");
                    return;
                }
                orderDAO.delete(Command.class.getDeclaredField("idclient"), selectedClient[0]);
                clientDAO.delete(Client.class.getDeclaredField("id"), selectedClient[0]);
            } catch (NoSuchFieldException noSuchFieldException) {
                noSuchFieldException.printStackTrace();
            }
            frame.updateClientTable();
        });

        frame.addAddProductListener(e -> {
            try{
                String name = frame.getProdName();
                float price = frame.getPrice();
                int stock = frame.getStock();
                if (price<0 || stock<0){
                    JOptionPane.showMessageDialog(null,"Invalid data!");
                    return;
                }
                productDAO.insert(new Product(name,price,stock));
                frame.updateProductTable();
            }
            catch(NumberFormatException nfe){
                JOptionPane.showMessageDialog(null,"Invalid data!");
            }
        });

        frame.addSelectProductListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) { selectedProduct[0] = frame.getSelectedProduct(); }
        });

        frame.addEditProductListener(e -> {
            try {
                if (selectedProduct[0] == -1){
                    JOptionPane.showMessageDialog(null,"Select a product!");
                    return;
                }

                String name = frame.getProdName();
                float price = frame.getPrice();
                int stock = frame.getStock();
                if (price<0 || stock<0){
                    JOptionPane.showMessageDialog(null,"Invalid data!");
                    return;
                }
                productDAO.update(selectedProduct[0],Product.class.getDeclaredField("name"),name);
                productDAO.update(selectedProduct[0],Product.class.getDeclaredField("price"),price);
                productDAO.update(selectedProduct[0],Product.class.getDeclaredField("stock"),stock);
            }catch(NumberFormatException nfe){
                JOptionPane.showMessageDialog(null,"Invalid data!");
            }catch (NoSuchFieldException noSuchFieldException) {
                noSuchFieldException.printStackTrace();
            }
            frame.updateProductTable();
        });

        frame.addDeleteProductListener(e -> {
            try {
                if (selectedProduct[0] == -1){
                    JOptionPane.showMessageDialog(null,"Select a product!");
                    return;
                }
                orderDAO.delete(Command.class.getDeclaredField("idproduct"), selectedProduct[0]);
                productDAO.delete(Client.class.getDeclaredField("id"), selectedProduct[0]);
            } catch (NoSuchFieldException noSuchFieldException) {
                noSuchFieldException.printStackTrace();
            }
            frame.updateProductTable();
        });

        frame.addAddOrderListener(e -> {
            try{
                if (selectedClient[0] == -1 || selectedProduct[0] == -1){
                    JOptionPane.showMessageDialog(null,"Select a client and a product!");
                    return;
                }
                int quantity = frame.getQuantity();
                if (quantity <= 0){
                    JOptionPane.showMessageDialog(null,"Negative or null quantity!");
                    return;
                }
                Product product = productDAO.findById(selectedProduct[0]);
                if (quantity>product.getStock()){
                    JOptionPane.showMessageDialog(null,"Not enough quantity!");
                    return;
                }
                int newKey = orderDAO.insert(new Command(selectedClient[0], selectedProduct[0], quantity));
                productDAO.update(selectedProduct[0],Product.class.getDeclaredField("stock"),product.getStock()-quantity);
                frame.updateProductTable();
                File f = new File("bills/bill "+newKey+".txt");
                if (!f.createNewFile())
                    return;
                FileWriter fw = new FileWriter(f);
                float totalCost = product.getPrice()*(float)quantity;
                fw.write(product.getName()+" - "+product.getPrice()+" x "+quantity+" = "+totalCost+"\nOrder made on "+ LocalDateTime.now());
                fw.close();

            }catch (NumberFormatException nfe){
                JOptionPane.showMessageDialog(null,"Invalid data!");
            } catch (NoSuchFieldException | IOException e1) {
                e1.printStackTrace();
            }
        });
    }
}
