/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * ProductDAO Class
 * @Date: April 17, 2021
 * @Source: https://stackoverflow.com/questions/29189895/how-to-resolve-a-java-lang-instantiationexception
 */

package model;

public class Client {
    private int id;
    private String name;

    public Client(String name) {
        this.name = name;
    }

    public Client() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
