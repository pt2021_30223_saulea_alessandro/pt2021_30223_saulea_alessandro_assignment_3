/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * ProductDAO Class
 * @Date: April 17, 2021
 * @Source: https://stackoverflow.com/questions/29189895/how-to-resolve-a-java-lang-instantiationexception
 */

package model;

public class Product {
    private int id;
    private String name;
    private float price;
    private int stock;

    public Product(String name, float price, int stock) {
        this.name = name;
        this.price = price;
        this.stock = stock;
    }

    public Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
