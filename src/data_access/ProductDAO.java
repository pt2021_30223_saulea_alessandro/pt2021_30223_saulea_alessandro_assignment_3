/**
 * @Author: Saulea Alessandro, student at TUCN, Computer Science, license grade 2
 * ProductDAO Class
 * @Date: April 17, 2021
 * @Source: https://teams.microsoft.com/_?lm=deeplink&lmsrc=homePageWeb&cmpid=WebSignIn#/pdf/viewer/teams/https:~2F~2Fdi
 * datec.sharepoint.com~2Fsites~2FTP274~2FClass%20Materials~2FLaborator~2FTema%203~2FASSIGNMENT_3_SUPPORT_PRESENTATION.p
 * df?threadId=19:4fed5c2e262a4057b1dabca3437d3a19@thread.tacv2&baseUrl=https:~2F~2Fdidatec.sharepoint.com~2Fsites~2FTP2
 * 74&fileId=615bfd47-ecf2-4d39-bf9f-cdc591b3048d&ctx=files&rootContext=items_view&viewerAction=view
 */

package data_access;

import model.*;

public class ProductDAO extends AbstractDAO<Product>{
}
